# Repeater script for Logitech G602 mouse

**Make Logitech G602 Great Again!**

Snatched from somewhere and polished afterwards to resemble MX series experience.

_Press a button → Single reaction → Delay (default is 400 milliseconds) → Repeated reaction (default is 40 milliseconds) → Release button_