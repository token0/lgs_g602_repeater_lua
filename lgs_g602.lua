-- all special functions must be defined BEFORE being assigned to a button

-- start/stop a repeat key task
function fn_Repeat(event, gkey, family, delay, ...)
	local taskname = "Repeat" .. gkey
	if event == "G_PRESSED" or event == "MOUSE_BUTTON_PRESSED" then
		if TaskRunning(taskname) then AbortTask(taskname) end
		RunTask(taskname, t_Repeat, delay, ...)
	elseif event == "G_RELEASED" or event == "MOUSE_BUTTON_RELEASED" then
		StopTask(taskname)
	end
end

-- t_Repeat is the function that actually performs the repeat task
function t_Repeat(delay, ...)
	local f = true
	local t1, t2 = 20, delay - 20
	local t3 = 400 -- delay before repeat task in milliseconds
	if t2 <= 0 then t1, t2 = delay / 2, delay / 2 end
	while f do
		f = TaskSleep(t3)
		while f do 
			PressKey(...)
			f = TaskSleep(t1)
			ReleaseKey(...)
			f = f and TaskSleep(t2)
		end
	end
end


-- G602 Mouse Bindings

G602 = {[0] = {}}

G602[1] = {
	[10] = {fn_Repeat, 40, 0x130},	-- 0x130 is the keycode for 'vol up', 40ms delay = 25 repeats/sec
	[11] = {fn_Repeat, 40, 0x12e},	-- 0x12e is the keycode for 'vol down', 40ms delay = 25 repeats/sec
}


-- G13 Advanced Gameboard Bindings

G13 = {[0] = {}}

G13[1] = {	-- Mode M1
}

G13[2] = {	-- Mode M2
}

G13[3] = {	-- Mode M3
}


-- G510 Keyboard Bindings

G510 = {[0] = {}}

G510[1] = {	-- Mode M1
}

G510[2] = {	-- Mode M2
}

G510[3] = {	-- Mode M3
}


-- G930 Headset Bindings

G930 = {[0] = {}}


-- Choose which device(s) you are using.  The LGS Lua environment supports only one device for each category.

Map = {
	["lhc"] = G13,
	["kb"] = G510,
	["mouse"] = G602,
	[""] = G930,
}


-- everything below this line is "framework" code and shouldn't normally be changed

function OnEvent(event, arg, family)
	local st = StateTimer
	if event == "G_PRESSED" or event == "MOUSE_BUTTON_PRESSED" then
		local mode = GetMKeyState(family)
		local map = Map[family][mode] or {}
		local action = map[arg]
		local t = type(action)
		if t == "function" then
			action(event, arg, family)
		elseif t == "table" then
			local f = action[1]
			if type(f) == "function" then
				f(event, arg, family, unpack(action, 2))
			else
				PressKey(unpack(action))
			end
		elseif t == "string" or t == "number" then
			PressKey(action)
		end
		Map[family][0][arg] = action
	elseif event == "G_RELEASED" or event == "MOUSE_BUTTON_RELEASED" then
		local map = Map[family][0]
		local action = map[arg]
		local t = type(action)
		if t == "function" then
			action(event, arg, family)
		elseif t == "table" then
			local f = action[1]
			if type(f) == "function" then
				f(event, arg, family, unpack(action, 2))
			else
				ReleaseKey(unpack(action))
			end
		elseif t == "string" or t == "number" then
			ReleaseKey(action)
		end
		map[arg] = nil
	elseif event == "PROFILE_ACTIVATED" then
		EnablePrimaryMouseButtonEvents(true)
		InitPolling()
	elseif event == "PROFILE_DEACTIVATED" then
		ReleaseAllKeys()
	end
	DoTasks()
	Poll(event, arg, family, st)
end

POLL_FAMILY = "mouse"	-- current mice don't have M-states, so this is a good choice
POLL_INTERVAL = 10	-- delay (in milliseconds) before next loop, used to throttle polling rate
POLL_DEADTIME = 100	-- settling time (in milliseconds) during which old poll events are drained

function InitPolling()
	ActiveState = GetMKeyState_Hook(POLL_FAMILY)
	SetMKeyState_Hook(ActiveState, POLL_FAMILY)
end

function Poll(event, arg, family, st)
	if st == nil and StateTimer ~= nil then return end
	local t = GetRunningTime()
	if family == POLL_FAMILY then
		if event == "M_PRESSED" and arg ~= ActiveState then
			if StateTimer ~= nil and t >= StateTimer then StateTimer = nil end
			if StateTimer == nil then ActiveState = arg end
			StateTimer = t + POLL_DEADTIME
		elseif event == "M_RELEASED" and arg == ActiveState then
			Sleep(POLL_INTERVAL)
			SetMKeyState_Hook(ActiveState, POLL_FAMILY)
		end
	end
end

GetMKeyState_Hook = GetMKeyState
GetMKeyState = function(family)
	family = family or "kb"
	if family == POLL_FAMILY then
		return ActiveState
	else
		return GetMKeyState_Hook(family)
	end
end

SetMKeyState_Hook = SetMKeyState
SetMKeyState = function(mkey, family)
	family = family or "kb"
	if family == POLL_FAMILY then
		if mkey == ActiveState then return end
		ActiveState = mkey
		StateTimer = GetRunningTime() + POLL_DEADTIME
	end
	return SetMKeyState_Hook(mkey, family)
end


-- Task Management functions

TaskList = {}

function DoTasks()
	local t = GetRunningTime()
	for key, task in pairs(TaskList) do
		if t >= task.time then
			local s, d = coroutine.resume(task.task, task.run)
			if (not s) or ((d or -1) < 0) then
				TaskList[key] = nil
			else
				task.time = task.time + d
			end
		end
	end
end

function RunTask(key, func, ...)
	AbortTask(key)
	local task = {}
	task.time = GetRunningTime()
	task.task = coroutine.create(func)
	task.run = true
	local s, d = coroutine.resume(task.task, ...)
	if (s) and ((d or -1) >= 0) then
		task.time = task.time + d
		TaskList[key] = task
	end
end

function StopTask(key)
	local task = TaskList[key]
	if task ~= nil then task.run = false end
end

function AbortTask(key)
	local task = TaskList[key]
	if task == nil then return end
	while true do
		local s, d = coroutine.resume(task.task, false)
		if (not s) or ((d or -1) < 0) then
			TaskList[key] = nil
			return
		end
	end
end

function TaskRunning(key)
	local task = TaskList[key]
	if task == nil then return false end
	return task.run
end

function TaskSleep(delay)
	return coroutine.yield(delay)
end


-- PressKey / ReleaseKey enhancements
-- if PressKey is called n times for a key, don't release until ReleaseKey has been called n times
-- allow arguments to be tables
-- release keys in reverse order of how they were pressed
-- allow "_", "^" and "@" prefixes for key-specific Shift, Ctrl and Alt modifiers
-- allow "mb1" through "mb5" for mouse buttons

MODIFIER_PREDELAY = 20	-- 20ms delay to allow Shift, Ctrl or Alt to register
MODIFIER_POSTDELAY = 10	-- 10ms delay before releasing Shift, Ctrl or Alt

KeyCounts = {}

PressKey_Hook = PressKey
PressKey = function(...)
	local n = select("#", ...)
	for i = 1, n do
		local arg = select(i, ...)
		if type(arg) == "table" then
			PressKey(unpack(arg))
		elseif arg ~= nil then
			local fShift, fCtrl, fAlt
			local c = string.sub(arg, 1, 1)
			while c == "_" or c == "^" or c == "@" do
				if c == "_" then
					fShift = true
				elseif c == "^" then
					fCtrl = true
				else
					fAlt = true
				end
				arg = string.sub(arg, 2)
				c = string.sub(arg, 1, 1)
			end
			c = KeyCounts[arg] or 0
			c = c + 1
			if c == 1 then
				if fShift then PressKey_Hook("lshift") end
				if fCtrl then PressKey_Hook("lctrl") end
				if fAlt then PressKey_Hook("lalt") end
				if fShift or fCtrl or fAlt then Sleep(MODIFIER_PREDELAY) end
				if string.sub(arg, 1, 2) == "mb" then
					arg = string.sub(arg, 3)
					PressMouseButton(arg)
				else
					PressKey_Hook(arg)
				end
				if fShift or fCtrl or fAlt then Sleep(MODIFIER_POSTDELAY) end
				if fAlt then ReleaseKey_Hook("lalt") end
				if fCtrl then ReleaseKey_Hook("lctrl") end
				if fShift then ReleaseKey_Hook("lshift") end
			end
			KeyCounts[arg] = c
		end
	end
end

ReleaseKey_Hook = ReleaseKey
ReleaseKey = function(...)
	local n = select("#", ...)
	for i = n, 1, -1 do
		local arg = select(i, ...)
		if type(arg) == "table" then
			ReleaseKey(unpack(arg))
		elseif arg ~= nil then
			local c = string.sub(arg, 1, 1)
			while c == "_" or c == "^" or c == "@" do
				arg = string.sub(arg, 2)
				c = string.sub(arg, 1, 1)
			end
			c = KeyCounts[arg] or 1
			c = c - 1
			if c == 0 then
				c = nil
				if string.sub(arg, 1, 2) == "mb" then
					arg = string.sub(arg, 3)
					ReleaseMouseButton(arg)
				else
					ReleaseKey_Hook(arg)
				end
			end
			KeyCounts[arg] = c
		end
	end
end

function ReleaseAllKeys()
	for arg, _ in pairs(KeyCounts) do
		KeyCounts[arg] = nil
		if string.sub(arg, 1, 2) == "mb" then
			arg = string.sub(arg, 3)
			ReleaseMouseButton(arg)
		else
			ReleaseKey_Hook(arg)
		end
	end
end